#ifndef EMBED_COMMANDS_HPP
#define EMBED_COMMANDS_HPP
// #include <fmt/format.h>
// #include <rip/misc/logger.hpp>
#include <rip/components/roboclaw.hpp>
#include <tuple>
#include <stdint.h>
#include <EmbMessenger/Command.hpp>
// #include <json.hpp>
// #include <units/units.hpp>
#include <EmbMessenger/EmbMessenger.hpp>

#define ROBOCLAW_ARDUINO_HPP
using emb::host::Command;
using emb::host::EmbMessenger;
namespace commands {
    /*
    class ForwardM1 : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        ForwardM1(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class BackwardM1 : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        BackwardM1(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class SetMinVoltageMainBattery : public Command
    {
        uint8_t m_voltage;
        // Parameter members
        bool m_rv;

    public:
        SetMinVoltageMainBattery(uint8_t voltage);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getVoltage() const;
    };

    class SetMaxVoltageMainBattery : public Command
    {
        uint8_t m_voltage;
        // Parameter members
        bool m_rv;

    public:
        SetMaxVoltageMainBattery(uint8_t voltage);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getVoltage() const;
    };

    class ForwardM2 : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        ForwardM2(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class BackwardM2 : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        BackwardM2(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class ForwardBackwardM1 : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        ForwardBackwardM1(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class ForwardBackwardM2 : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        ForwardBackwardM2(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class ForwardMixed : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        ForwardMixed(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class BackwardMixed : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        BackwardMixed(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class TurnRightMixed : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        TurnRightMixed(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class TurnLeftMixed : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        TurnLeftMixed(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class ForwardBackwardMixed : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        ForwardBackwardMixed(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class LeftRightMixed : public Command
    {
        uint8_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        LeftRightMixed(uint8_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getSpeed() const;
    };

    class ReadEncM1 : public Command
    {
        uint8_t* m_status;
        bool* m_valid;
        // Parameter members
        uint32_t m_rv;

    public:
        ReadEncM1(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t* getStatus() const;
        bool* getValid() const;
    };

    class ReadEncM2 : public Command
    {
        uint8_t* m_status;
        bool* m_valid;
        // Parameter members
        uint32_t m_rv;

    public:
        ReadEncM2(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t* getStatus() const;
        bool* getValid() const;
    };

    class SetEncM1 : public Command
    {
        int32_t m_val;
        // Parameter members
        bool m_rv;

    public:
        SetEncM1(int32_t val);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        int32_t getVal() const;
    };

    class SetEncM2 : public Command
    {
        int32_t m_val;
        // Parameter members
        bool m_rv;

    public:
        SetEncM2(int32_t val);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        int32_t getVal() const;
    };

    class ReadSpeedM1 : public Command
    {
        uint8_t* m_status;
        bool* m_valid;
        // Parameter members
        uint32_t m_rv;

    public:
        ReadSpeedM1(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t* getStatus() const;
        bool* getValid() const;
    };

    class ReadSpeedM2 : public Command
    {
        uint8_t* m_status;
        bool* m_valid;
        // Parameter members
        uint32_t m_rv;

    public:
        ReadSpeedM2(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t* getStatus() const;
        bool* getValid() const;
    };

    class ResetEncoders : public Command
    {
        uint8_t m_address;
        // Parameter members
        bool m_rv;

    public:
        ResetEncoders(uint8_t address);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getAddress() const;
    };
    /*
    class ReadVersion : public Command
    {
        char* m_version;
        // Parameter members
        bool m_rv;

    public:
        ReadVersion(char* version);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        char* getVersion() const;
    };

    class ReadMainBatteryVoltage : public Command
    {
        bool* m_valid;
        // Parameter members
        uint16_t m_rv;

    public:
        ReadMainBatteryVoltage(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        bool* getValid() const;
    };

    class ReadLogicBatteryVoltage : public Command
    {
        bool* m_valid;
        // Parameter members
        uint16_t m_rv;

    public:
        ReadLogicBatteryVoltage(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        bool* getValid() const;
    };

    class SetMinVoltageLogicBattery : public Command
    {
        uint8_t m_voltage;
        // Parameter members
        bool m_rv;

    public:
        SetMinVoltageLogicBattery(uint8_t voltage);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getVoltage() const;
    };

    class SetMaxVoltageLogicBattery : public Command
    {
        uint8_t m_voltage;
        // Parameter members
        bool m_rv;

    public:
        SetMaxVoltageLogicBattery(uint8_t voltage);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getVoltage() const;
    };

    class SetM1VelocityPID : public Command
    {
        float m_Kp;
        float m_Ki;
        float m_Kd;
        uint32_t m_qpps;
        // Parameter members
        bool m_rv;

    public:
        SetM1VelocityPID(float Kp, float Ki, uint32_t qpps);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp() const;
        float getKi() const;
        float getKd() const;
        uint32_t getQpps() const;
    };

    class SetM2VelocityPID : public Command
    {
        float m_Kp;
        float m_Ki;
        float m_Kd;
        uint32_t m_qpps;
        // Parameter members
        bool m_rv;

    public:
        SetM2VelocityPID(float Kp, float Ki, uint32_t qpps);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp() const;
        float getKi() const;
        float getKd() const;
        uint32_t getQpps() const;
    };

    class ReadISpeedM1 : public Command
    {
        uint8_t m_status;
        bool m_valid;
        // Parameter members
        uint32_t m_rv;

    public:
        ReadISpeedM1(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getStatus() const;
        bool getValid() const;
    };

    class ReadISpeedM2 : public Command
    {
        uint8_t m_status;
        bool m_valid;
        // Parameter members
        uint32_t m_rv;

    public:
        ReadISpeedM2(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t* getStatus() const;
        bool* getValid() const;
    };
    */

    class DutyM1 : public Command
    {
        uint8_t m_address;
        uint16_t m_duty;

    public:
        DutyM1(uint8_t address, uint16_t duty);
        void send(EmbMessenger* messenger);
    };

    class DutyM2 : public Command
    {
        uint8_t m_address;
        uint16_t m_duty;

    public:
        DutyM2(uint8_t address, uint16_t duty);
        void send(EmbMessenger* messenger);
    };

    class DutyM1M2 : public Command
    {
        uint8_t m_address;
        uint16_t m_duty1;
        uint16_t m_duty2;

    public:
        DutyM1M2(uint8_t address, uint16_t duty1, uint16_t duty2);
        void send(EmbMessenger* messenger);
    };
    /*
    class SpeedM1 : public Command
    {
        uint32_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        SpeedM1(uint32_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getSpeed() const;
    };

    class SpeedM2 : public Command
    {
        uint32_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        SpeedM2(uint32_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getSpeed() const;
    };

    class SpeedM1M2 : public Command
    {
        uint32_t m_speed1;
        uint32_t m_speed2;
        // Parameter members
        bool m_rv;

    public:
        SpeedM1M2(uint32_t speed2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getSpeed1() const;
        uint32_t getSpeed2() const;
    };

    class SpeedAccelM1 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelM1(uint32_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed() const;
    };

    class SpeedAccelM2 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelM2(uint32_t speed);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed() const;
    };

    class SpeedAccelM1M2 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed1;
        uint32_t m_speed2;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelM1M2(uint32_t accel, uint32_t speed2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed1() const;
        uint32_t getSpeed2() const;
    };

    class SpeedDistanceM1 : public Command
    {
        uint32_t m_speed;
        uint32_t m_distance;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedDistanceM1(uint32_t speed, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getSpeed() const;
        uint32_t getDistance() const;
        uint8_t getFlag() const;
    };

    class SpeedDistanceM2 : public Command
    {
        uint32_t m_speed;
        uint32_t m_distance;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedDistanceM2(uint32_t speed, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getSpeed() const;
        uint32_t getDistance() const;
        uint8_t getFlag() const;
    };

    class SpeedDistanceM1M2 : public Command
    {
        uint32_t m_speed1;
        uint32_t m_distance1;
        uint32_t m_speed2;
        uint32_t m_distance2;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedDistanceM1M2(uint32_t speed1, uint32_t distance1, uint32_t speed2, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getSpeed1() const;
        uint32_t getDistance1() const;
        uint32_t getSpeed2() const;
        uint32_t getDistance2() const;
        uint8_t getFlag() const;
    };

    class SpeedAccelDistanceM1 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed;
        uint32_t m_distance;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelDistanceM1(uint32_t accel, uint32_t speed, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed() const;
        uint32_t getDistance() const;
        uint8_t getFlag() const;
    };

    class SpeedAccelDistanceM2 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed;
        uint32_t m_distance;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelDistanceM2(uint32_t accel, uint32_t speed, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed() const;
        uint32_t getDistance() const;
        uint8_t getFlag() const;
    };

    class SpeedAccelDistanceM1M2 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed1;
        uint32_t m_distance1;
        uint32_t m_speed2;
        uint32_t m_distance2;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelDistanceM1M2(uint32_t accel, uint32_t speed1, uint32_t distance1, uint32_t speed2, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed1() const;
        uint32_t getDistance1() const;
        uint32_t getSpeed2() const;
        uint32_t getDistance2() const;
        uint8_t getFlag() const;
    };

    class ReadBuffers : public Command
    {
        uint8_t m_depth1;
        uint8_t m_depth2;
        // Parameter members
        bool m_rv;

    public:
        ReadBuffers(uint8_t depth2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getDepth1() const;
        uint8_t getDepth2() const;
    };
    /*
    class ReadPWMs : public Command
    {
        int16_t m_pwm1;
        int16_t m_pwm2;
        // Parameter members
        bool m_rv;

    public:
        ReadPWMs(int16_t pwm2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        int16_t getPwm1() const;
        int16_t getPwm2() const;
    };

    class ReadCurrents : public Command
    {
        int16_t m_current1;
        int16_t m_current2;
        // Parameter members
        bool m_rv;

    public:
        ReadCurrents(int16_t current2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        int16_t getCurrent1() const;
        int16_t getCurrent2() const;
    };

    class SpeedAccelM1M2_2 : public Command
    {
        uint32_t m_accel1;
        uint32_t m_speed1;
        uint32_t m_accel2;
        uint32_t m_speed2;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelM1M2_2(uint32_t accel1, uint32_t speed1, uint32_t speed2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel1() const;
        uint32_t getSpeed1() const;
        uint32_t getAccel2() const;
        uint32_t getSpeed2() const;
    };

    class SpeedAccelDistanceM1M2_2 : public Command
    {
        uint32_t m_accel1;
        uint32_t m_speed1;
        uint32_t m_distance1;
        uint32_t m_accel2;
        uint32_t m_speed2;
        uint32_t m_distance2;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelDistanceM1M2_2(uint32_t accel1,
                                uint32_t speed1,
                                uint32_t distance1,
                                uint32_t accel2,
                                uint32_t speed2,
                                uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel1() const;
        uint32_t getSpeed1() const;
        uint32_t getDistance1() const;
        uint32_t getAccel2() const;
        uint32_t getSpeed2() const;
        uint32_t getDistance2() const;
        uint8_t getFlag() const;
    };

    class DutyAccelM1 : public Command
    {
        uint16_t m_duty;
        uint32_t m_accel;
        // Parameter members
        bool m_rv;

    public:
        DutyAccelM1(uint32_t accel);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getDuty() const;
        uint32_t getAccel() const;
    };

    class DutyAccelM2 : public Command
    {
        uint16_t m_duty;
        uint32_t m_accel;
        // Parameter members
        bool m_rv;

    public:
        DutyAccelM2(uint32_t accel);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getDuty() const;
        uint32_t getAccel() const;
    };

    class DutyAccelM1M2 : public Command
    {
        uint16_t m_duty1;
        uint32_t m_accel1;
        uint16_t m_duty2;
        uint32_t m_accel2;
        // Parameter members
        bool m_rv;

    public:
        DutyAccelM1M2(uint16_t duty1, uint32_t accel1, uint32_t accel2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getDuty1() const;
        uint32_t getAccel1() const;
        uint16_t getDuty2() const;
        uint32_t getAccel2() const;
    };

    class ReadM1VelocityPID : public Command
    {
        float m_Kp_fp;
        float m_Ki_fp;
        float m_Kd_fp;
        uint32_t m_qpps;
        // Parameter members
        bool m_rv;

    public:
        ReadM1VelocityPID(float Kp_fp, float Ki_fp, uint32_t qpps);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp_fp() const;
        float getKi_fp() const;
        float getKd_fp() const;
        uint32_t getQpps() const;
    };

    class ReadM2VelocityPID : public Command
    {
        float m_Kp_fp;
        float m_Ki_fp;
        float m_Kd_fp;
        uint32_t m_qpps;
        // Parameter members
        bool m_rv;

    public:
        ReadM2VelocityPID(float Kp_fp, float Ki_fp, uint32_t qpps);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp_fp() const;
        float getKi_fp() const;
        float getKd_fp() const;
        uint32_t getQpps() const;
    };

    class SetMainVoltages : public Command
    {
        uint16_t m_min;
        uint16_t m_max;
        // Parameter members
        bool m_rv;

    public:
        SetMainVoltages(uint16_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getMin() const;
        uint16_t getMax() const;
    };

    class SetLogicVoltages : public Command
    {
        uint16_t m_min;
        uint16_t m_max;
        // Parameter members
        bool m_rv;

    public:
        SetLogicVoltages(uint16_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getMin() const;
        uint16_t getMax() const;
    };

    class ReadMinMaxMainVoltages : public Command
    {
        uint16_t m_min;
        uint16_t m_max;
        // Parameter members
        bool m_rv;

    public:
        ReadMinMaxMainVoltages(uint16_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getMin() const;
        uint16_t getMax() const;
    };

    class ReadMinMaxLogicVoltages : public Command
    {
        uint16_t m_min;
        uint16_t m_max;
        // Parameter members
        bool m_rv;

    public:
        ReadMinMaxLogicVoltages(uint16_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getMin() const;
        uint16_t getMax() const;
    };

    class SetM1PositionPID : public Command
    {
        float m_kp;
        float m_ki;
        float m_kd;
        uint32_t m_kiMax;
        uint32_t m_deadzone;
        uint32_t m_min;
        uint32_t m_max;
        // Parameter members
        bool m_rv;

    public:
        SetM1PositionPID(float kp, float ki, float kd, uint32_t kiMax, uint32_t deadzone, uint32_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp() const;
        float getKi() const;
        float getKd() const;
        uint32_t getKiMax() const;
        uint32_t getDeadzone() const;
        uint32_t getMin() const;
        uint32_t getMax() const;
    };

    class SetM2PositionPID : public Command
    {
        float m_kp;
        float m_ki;
        float m_kd;
        uint32_t m_kiMax;
        uint32_t m_deadzone;
        uint32_t m_min;
        uint32_t m_max;
        // Parameter members
        bool m_rv;

    public:
        SetM2PositionPID(float kp, float ki, float kd, uint32_t kiMax, uint32_t deadzone, uint32_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp() const;
        float getKi() const;
        float getKd() const;
        uint32_t getKiMax() const;
        uint32_t getDeadzone() const;
        uint32_t getMin() const;
        uint32_t getMax() const;
    };

    class ReadM1PositionPID : public Command
    {
        float m_Kp;
        float m_Ki;
        float m_Kd;
        uint32_t m_KiMax;
        uint32_t m_DeadZone;
        uint32_t m_Min;
        uint32_t m_Max;
        // Parameter members
        bool m_rv;

    public:
        ReadM1PositionPID(float Kp, float Ki, float Kd, uint32_t KiMax, uint32_t DeadZone, uint32_t Max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp() const;
        float getKi() const;
        float getKd() const;
        uint32_t getKiMax() const;
        uint32_t getDeadZone() const;
        uint32_t getMin() const;
        uint32_t getMax() const;
    };

    class ReadM2PositionPID : public Command
    {
        float m_Kp;
        float m_Ki;
        float m_Kd;
        uint32_t m_KiMax;
        uint32_t m_DeadZone;
        uint32_t m_Min;
        uint32_t m_Max;
        // Parameter members
        bool m_rv;

    public:
        ReadM2PositionPID(float Kp, float Ki, float Kd, uint32_t KiMax, uint32_t DeadZone, uint32_t Max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        float getKp() const;
        float getKi() const;
        float getKd() const;
        uint32_t getKiMax() const;
        uint32_t getDeadZone() const;
        uint32_t getMin() const;
        uint32_t getMax() const;
    };

    class SpeedAccelDeccelPositionM1 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed;
        uint32_t m_deccel;
        uint32_t m_position;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelDeccelPositionM1(uint32_t accel, uint32_t speed, uint32_t deccel, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed() const;
        uint32_t getDeccel() const;
        uint32_t getPosition() const;
        uint8_t getFlag() const;
    };

    class SpeedAccelDeccelPositionM2 : public Command
    {
        uint32_t m_accel;
        uint32_t m_speed;
        uint32_t m_deccel;
        uint32_t m_position;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelDeccelPositionM2(uint32_t accel, uint32_t speed, uint32_t deccel, uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
        uint32_t getSpeed() const;
        uint32_t getDeccel() const;
        uint32_t getPosition() const;
        uint8_t getFlag() const;
    };

    class SpeedAccelDeccelPositionM1M2 : public Command
    {
        uint32_t m_accel1;
        uint32_t m_speed1;
        uint32_t m_deccel1;
        uint32_t m_position1;
        uint32_t m_accel2;
        uint32_t m_speed2;
        uint32_t m_deccel2;
        uint32_t m_position2;
        uint8_t m_flag;
        // Parameter members
        bool m_rv;

    public:
        SpeedAccelDeccelPositionM1M2(uint32_t accel1,
                                    uint32_t speed1,
                                    uint32_t deccel1,
                                    uint32_t position1,
                                    uint32_t accel2,
                                    uint32_t speed2,
                                    uint32_t deccel2,
                                    uint8_t flag);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel1() const;
        uint32_t getSpeed1() const;
        uint32_t getDeccel1() const;
        uint32_t getPosition1() const;
        uint32_t getAccel2() const;
        uint32_t getSpeed2() const;
        uint32_t getDeccel2() const;
        uint32_t getPosition2() const;
        uint8_t getFlag() const;
    };

    class SetM1DefaultAccel : public Command
    {
        uint32_t m_accel;
        // Parameter members
        bool m_rv;

    public:
        SetM1DefaultAccel(uint32_t accel);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
    };

    class SetM2DefaultAccel : public Command
    {
        uint32_t m_accel;
        // Parameter members
        bool m_rv;

    public:
        SetM2DefaultAccel(uint32_t accel);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getAccel() const;
    };

    class SetPinFunctions : public Command
    {
        uint8_t m_S3mode;
        uint8_t m_S4mode;
        uint8_t m_S5mode;
        // Parameter members
        bool m_rv;

    public:
        SetPinFunctions(uint8_t S3mode, uint8_t S5mode);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getS3mode() const;
        uint8_t getS4mode() const;
        uint8_t getS5mode() const;
    };

    class GetPinFunctions : public Command
    {
        uint8_t m_S3mode;
        uint8_t m_S4mode;
        uint8_t m_S5mode;
        // Parameter members
        bool m_rv;

    public:
        GetPinFunctions(uint8_t S3mode, uint8_t S5mode);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getS3mode() const;
        uint8_t getS4mode() const;
        uint8_t getS5mode() const;
    };

    class SetDeadBand : public Command
    {
        uint8_t m_Min;
        uint8_t m_Max;
        // Parameter members
        bool m_rv;

    public:
        SetDeadBand(uint8_t Max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getMin() const;
        uint8_t getMax() const;
    };

    class GetDeadBand : public Command
    {
        uint8_t m_Min;
        uint8_t m_Max;
        // Parameter members
        bool m_rv;

    public:
        GetDeadBand(uint8_t Max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getMin() const;
        uint8_t getMax() const;
    };

    class ReadEncoders : public Command
    {
        uint32_t m_enc1;
        uint32_t m_enc2;
        // Parameter members
        bool m_rv;

    public:
        ReadEncoders(uint32_t enc2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getEnc1() const;
        uint32_t getEnc2() const;
    };

    class ReadISpeeds : public Command
    {
        uint32_t m_ispeed1;
        uint32_t m_ispeed2;
        // Parameter members
        bool m_rv;

    public:
        ReadISpeeds(uint32_t ispeed2);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getIspeed1() const;
        uint32_t getIspeed2() const;
    };

    class RestoreDefaults : public Command
    {
        uint8_t m_address;
        // Parameter members
        bool m_rv;

    public:
        RestoreDefaults(uint8_t address);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getAddress() const;
    };

    class ReadTemp : public Command
    {
        uint16_t m_temp;
        // Parameter members
        bool m_rv;

    public:
        ReadTemp(uint16_t temp);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getTemp() const;
    };

    class ReadTemp2 : public Command
    {
        uint16_t m_temp;
        // Parameter members
        bool m_rv;

    public:
        ReadTemp2(uint16_t temp);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getTemp() const;
    };

    class ReadError : public Command
    {
        bool* m_valid;
        // Parameter members
        uint16_t m_rv;

    public:
        ReadError(bool* valid);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        bool* getValid() const;
    };

    class ReadEncoderModes : public Command
    {
        uint8_t m_M1mode;
        uint8_t m_M2mode;
        // Parameter members
        bool m_rv;

    public:
        ReadEncoderModes(uint8_t M2mode);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getM1mode() const;
        uint8_t getM2mode() const;
    };

    class SetM1EncoderMode : public Command
    {
        uint8_t m_mode;
        // Parameter members
        bool m_rv;

    public:
        SetM1EncoderMode(uint8_t mode);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getMode() const;
    };

    class SetM2EncoderMode : public Command
    {
        uint8_t m_mode;
        // Parameter members
        bool m_rv;

    public:
        SetM2EncoderMode(uint8_t mode);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getMode() const;
    };

    class WriteNVM : public Command
    {
        uint8_t m_address;
        // Parameter members
        bool m_rv;

    public:
        WriteNVM(uint8_t address);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getAddress() const;
    };

    class ReadNVM : public Command
    {
        uint8_t m_address;
        // Parameter members
        bool m_rv;

    public:
        ReadNVM(uint8_t address);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getAddress() const;
    };

    class SetConfig : public Command
    {
        uint16_t m_config;
        // Parameter members
        bool m_rv;

    public:
        SetConfig(uint16_t config);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getConfig() const;
    };

    class GetConfig : public Command
    {
        uint16_t m_config;
        // Parameter members
        bool m_rv;

    public:
        GetConfig(uint16_t config);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint16_t getConfig() const;
    };

    class SetM1MaxCurrent : public Command
    {
        uint32_t m_max;
        // Parameter members
        bool m_rv;

    public:
        SetM1MaxCurrent(uint32_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getMax() const;
    };

    class SetM2MaxCurrent : public Command
    {
        uint32_t m_max;
        // Parameter members
        bool m_rv;

    public:
        SetM2MaxCurrent(uint32_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getMax() const;
    };

    class ReadM1MaxCurrent : public Command
    {
        uint32_t m_max;
        // Parameter members
        bool m_rv;

    public:
        ReadM1MaxCurrent(uint32_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getMax() const;
    };

    class ReadM2MaxCurrent : public Command
    {
        uint32_t m_max;
        // Parameter members
        bool m_rv;

    public:
        ReadM2MaxCurrent(uint32_t max);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getMax() const;
    };

    class SetPWMMode : public Command
    {
        uint8_t m_mode;
        // Parameter members
        bool m_rv;

    public:
        SetPWMMode(uint8_t mode);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getMode() const;
    };

    class GetPWMMode : public Command
    {
        uint8_t m_mode;
        // Parameter members
        bool m_rv;

    public:
        GetPWMMode(uint8_t mode);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getMode() const;
    };

    class available : public Command
    {};

    class begin : public Command
    {
        long m_speed;
        // Parameter members
    public:
        begin(long speed);
        void send(EmbMessenger* messenger);
        long getSpeed() const;
    };

    class end : public Command
    {};

    class read : public Command
    {};

    class read : public Command
    {
        uint32_t m_timeout;
        // Parameter members
        uint8_t m_rv;

    public:
        read(uint32_t timeout);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint32_t getTimeout() const;
    };

    class listen : public Command
    {};

    class write : public Command
    {
        uint8_t m_byte;
        // Parameter members
        size_t m_rv;

    public:
        write(uint8_t byte);
        void send(EmbMessenger* messenger);
        void receive(EmbMessenger* messenger);
        uint8_t getByte() const;
    };

    class flush : public Command
    {};

    class clear : public Command
    {};

    class stop : public Command
    {};

    class diagnostic : public Command
    {};
    */
}
#endif
