#ifndef ROBOCLAW_HPP
#define ROBOCLAW_HPP

#include <nlohmann/json.hpp>
#include <rip/robot_component.hpp>
#include <cstdint>
#include <string>

// TODO: Add in more user friendly functions, particularly units.
// TODO: replace c style by reference getter functions with rvalues
namespace rip::components
{
    class Roboclaw : public rip::RobotComponent
    {
    public:

        Roboclaw(const std::string& name, const nlohmann::json& config, 
        std::shared_ptr<emb::host::EmbMessenger> emb = nullptr, 
        std::shared_ptr<std::unordered_map<std::string, 
        std::shared_ptr<RobotComponent> > > comps = nullptr) 
        : RobotComponent(name, config, emb, comps) 
        {
            if(config.find("address") != config.end())
            {
                m_address = config.at("address");
            }
            else
            {
                m_address = 0x80;
            }
        }

        Roboclaw(){}

        // Roboclaw Lib, C style function prototypes
        
        virtual bool ForwardM1(uint8_t speed)                                                    = 0;

        virtual bool BackwardM1(uint8_t speed)                                                   = 0;

        virtual bool SetMinVoltageMainBattery(uint8_t voltage)                                   = 0;
        
        virtual bool SetMaxVoltageMainBattery(uint8_t voltage)                                   = 0;

        virtual bool ForwardM2(uint8_t speed)                                                    = 0;

        virtual bool BackwardM2(uint8_t speed)                                                   = 0;

        virtual bool ForwardBackwardM1(uint8_t speed)                                            = 0;
        
        virtual bool ForwardBackwardM2(uint8_t speed)                                            = 0;

        virtual bool ForwardMixed(uint8_t speed)                                                 = 0;
        
        virtual bool BackwardMixed(uint8_t speed)                                                = 0;
        
        virtual bool TurnRightMixed(uint8_t speed)                                               = 0;
        
        virtual bool TurnLeftMixed(uint8_t speed)                                                = 0;
        
        virtual bool ForwardBackwardMixed(uint8_t speed)                                         = 0;
        
        virtual bool LeftRightMixed(uint8_t speed)                                               = 0;
        
        virtual uint32_t ReadEncM1(uint8_t *status = NULL, bool *valid = NULL)                   = 0;
        
        virtual uint32_t ReadEncM2(uint8_t *status = NULL, bool *valid = NULL)                   = 0;
        
        virtual bool SetEncM1(int32_t val)                                                       = 0;
        
        virtual bool SetEncM2(int32_t val)                                                       = 0;
        
        virtual uint32_t ReadSpeedM1(uint8_t *status = NULL, bool *valid = NULL)                 = 0;
        
        virtual uint32_t ReadSpeedM2(uint8_t *status = NULL, bool *valid = NULL)                 = 0;
        
        virtual bool ResetEncoders(uint8_t address)                                                               = 0;
        
        virtual bool ReadVersion(char *version)                                                  = 0;
        
        virtual uint16_t ReadMainBatteryVoltage(bool *valid = NULL)                              = 0;
        
        virtual uint16_t ReadLogicBatteryVoltage(bool *valid = NULL)                             = 0;
        
        virtual bool SetMinVoltageLogicBattery(uint8_t voltage)                                  = 0;
        
        virtual bool SetMaxVoltageLogicBattery(uint8_t voltage)                                  = 0;
        
        virtual bool SetM1VelocityPID(float Kp, float Ki, float Kd, uint32_t qpps)               = 0;
        
        virtual bool SetM2VelocityPID(float Kp, float Ki, float Kd, uint32_t qpps)               = 0;
        
        virtual uint32_t ReadISpeedM1(uint8_t *status = NULL, bool *valid = NULL)                = 0;
        
        virtual uint32_t ReadISpeedM2(uint8_t *status = NULL, bool *valid = NULL)                = 0;
        
        virtual bool DutyM1(uint16_t duty)                                                       = 0;
        
        virtual bool DutyM2(uint16_t duty)                                                       = 0;
        
        virtual bool DutyM1M2(uint16_t duty1, uint16_t duty2)                                    = 0;
        
        virtual bool SpeedM1(uint32_t speed)                                                     = 0;
        
        virtual bool SpeedM2(uint32_t speed)                                                     = 0;
        
        virtual bool SpeedM1M2(uint32_t speed1, uint32_t speed2)                                 = 0;
        
        virtual bool SpeedAccelM1(uint32_t accel, uint32_t speed)                                = 0;
        
        virtual bool SpeedAccelM2(uint32_t accel, uint32_t speed)                                = 0;
        
        virtual bool SpeedAccelM1M2(uint32_t accel, uint32_t speed1, uint32_t speed2)            = 0;
        
        virtual bool SpeedDistanceM1(uint32_t speed, uint32_t distance, uint8_t flag = 0)        = 0;
        
        virtual bool SpeedDistanceM2(uint32_t speed, uint32_t distance, uint8_t flag = 0)        = 0;
        
        virtual bool SpeedDistanceM1M2(uint32_t speed1,
            uint32_t distance1,
            uint32_t speed2,
            uint32_t distance2,
            uint8_t flag = 0)                                                          = 0;
        virtual bool SpeedAccelDistanceM1(
                                            uint32_t accel,
                                            uint32_t speed,
                                            uint32_t distance,
                                            uint8_t flag = 0)                                                       = 0;
        virtual bool SpeedAccelDistanceM2(
                                            uint32_t accel,
                                            uint32_t speed,
                                            uint32_t distance,
                                            uint8_t flag = 0)                                                       = 0;
        virtual bool SpeedAccelDistanceM1M2(
                                            uint32_t accel,
                                            uint32_t speed1,
                                            uint32_t distance1,
                                            uint32_t speed2,
                                            uint32_t distance2,
                                            uint8_t flag = 0)                                                     = 0;
        
        virtual bool ReadBuffers(uint8_t &depth1, uint8_t &depth2)                               = 0;
        
        virtual bool ReadPWMs(int16_t &pwm1, int16_t &pwm2)                                      = 0;
        
        virtual bool ReadCurrents(int16_t &current1, int16_t &current2)                          = 0;
        
        virtual bool SpeedAccelM1M2_2(
                                        uint32_t accel1,
                                        uint32_t speed1,
                                        uint32_t accel2,
                                        uint32_t speed2)                                                            = 0;
        
        virtual bool SpeedAccelDistanceM1M2_2(
                                                uint32_t accel1,
                                                uint32_t speed1,
                                                uint32_t distance1,
                                                uint32_t accel2,
                                                uint32_t speed2,
                                                uint32_t distance2,
                                                uint8_t flag = 0)                                                   = 0;
        
        virtual bool DutyAccelM1(uint16_t duty, uint32_t accel)                                  = 0;
        
        virtual bool DutyAccelM2(uint16_t duty, uint32_t accel)                                  = 0;
        
        virtual bool DutyAccelM1M2(
                                    uint16_t duty1,
                                    uint32_t accel1,
                                    uint16_t duty2,
                                    uint32_t accel2)                                                               = 0;
        
        virtual bool ReadM1VelocityPID(float &Kp_fp, float &Ki_fp, float &Kd_fp, uint32_t &qpps) = 0;
        
        virtual bool ReadM2VelocityPID(float &Kp_fp, float &Ki_fp, float &Kd_fp, uint32_t &qpps) = 0;
        
        virtual bool SetMainVoltages(uint16_t min, uint16_t max)                                 = 0;
        
        virtual bool SetLogicVoltages(uint16_t min, uint16_t max)                                = 0;
        
        virtual bool ReadMinMaxMainVoltages(uint16_t &min, uint16_t &max)                        = 0;
        
        virtual bool ReadMinMaxLogicVoltages(uint16_t &min, uint16_t &max)                       = 0;
        
        virtual bool SetM1PositionPID(
        
                                        float kp,
                                        float ki,
                                        float kd,
                                        uint32_t kiMax,
                                        uint32_t deadzone,
                                        uint32_t min,
                                        uint32_t max)                                                               = 0;
        
        virtual bool SetM2PositionPID(
                                        float kp,
                                        float ki,
                                        float kd,
                                        uint32_t kiMax,
                                        uint32_t deadzone,
                                        uint32_t min,
                                        uint32_t max)                                                               = 0;
        
        virtual bool ReadM1PositionPID(
                                        float &Kp,
                                        float &Ki,
                                        float &Kd,
                                        uint32_t &KiMax,
                                        uint32_t &DeadZone,
                                        uint32_t &Min,
                                        uint32_t &Max)                                                             = 0;
        
        virtual bool ReadM2PositionPID(
                                        float &Kp,
                                        float &Ki,
                                        float &Kd,
                                        uint32_t &KiMax,
                                        uint32_t &DeadZone,
                                        uint32_t &Min,
                                        uint32_t &Max)                                                             = 0;
        
        virtual bool SpeedAccelDeccelPositionM1(
                                                uint32_t accel,
                                                uint32_t speed,
                                                uint32_t deccel,
                                                uint32_t position,
                                                uint8_t flag)                                                     = 0;
        
        virtual bool SpeedAccelDeccelPositionM2(
                                                uint32_t accel,
                                                uint32_t speed,
                                                uint32_t deccel,
                                                uint32_t position,
                                                uint8_t flag)                                                     = 0;
        
        virtual bool SpeedAccelDeccelPositionM1M2(
                                                    uint32_t accel1,
                                                    uint32_t speed1,
                                                    uint32_t deccel1,
                                                    uint32_t position1,
                                                    uint32_t accel2,
                                                    uint32_t speed2,
                                                    uint32_t deccel2,
                                                    uint32_t position2,
                                                    uint8_t flag)                                                   = 0;
        
        virtual bool SetM1DefaultAccel(uint32_t accel)                                           = 0;
        
        virtual bool SetM2DefaultAccel(uint32_t accel)                                           = 0;
        
        virtual bool SetPinFunctions(uint8_t S3mode, uint8_t S4mode, uint8_t S5mode)             = 0;
        
        virtual bool GetPinFunctions(uint8_t &S3mode, uint8_t &S4mode, uint8_t &S5mode)          = 0;
        
        virtual bool SetDeadBand(uint8_t Min, uint8_t Max)                                       = 0;
        
        virtual bool GetDeadBand(uint8_t &Min, uint8_t &Max)                                     = 0;
        
        virtual bool ReadEncoders(uint32_t &enc1, uint32_t &enc2)                                = 0;
        
        virtual bool ReadISpeeds(uint32_t &ispeed1, uint32_t &ispeed2)                           = 0;
        
        virtual bool RestoreDefaults(uint8_t address)                                                             = 0;
        
        virtual bool ReadTemp(uint16_t &temp)                                                    = 0;
        
        virtual bool ReadTemp2(uint16_t &temp)                                                   = 0;
        
        virtual uint16_t ReadError(bool *valid = NULL)                                           = 0;
        
        virtual bool ReadEncoderModes(uint8_t &M1mode, uint8_t &M2mode)                          = 0;
        
        virtual bool SetM1EncoderMode(uint8_t mode)                                              = 0;
        
        virtual bool SetM2EncoderMode(uint8_t mode)                                              = 0;
        
        virtual bool WriteNVM(uint8_t address)                                                                    = 0;
        
        virtual bool ReadNVM(uint8_t address)                                                                     = 0;
        
        virtual bool SetConfig(uint16_t config)                                                  = 0;
        
        virtual bool GetConfig(uint16_t &config)                                                 = 0;

        virtual bool SetM1MaxCurrent(uint32_t max)                                               = 0;
        
        virtual bool SetM2MaxCurrent(uint32_t max)                                               = 0;
        
        virtual bool ReadM1MaxCurrent(uint32_t &max)                                             = 0;
        
        virtual bool ReadM2MaxCurrent(uint32_t &max)                                             = 0;
        
        virtual bool SetPWMMode(uint8_t mode)                                                    = 0;

        virtual bool GetPWMMode(uint8_t &mode)                                                   = 0;


    protected:
        // True if arduino is host (dated term: appendage)
        bool m_type;
        std::string m_device;
        uint32_t m_timeout;
        uint8_t m_address;
        double m_ticks_per_rev;
        // units::Distance m_wheel_radius;
    };
}
#endif
