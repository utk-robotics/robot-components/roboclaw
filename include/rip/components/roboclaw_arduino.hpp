#ifndef ROBOCLAW_ARDUINO_HPP
#define ROBOCLAW_ARDUINO_HPP

// #include <fmt/format.h>

// #include <rip/misc/logger.hpp>
#include <tuple>
// #include <cmd_messenger/command.hpp>
// #include "appendages/appendage.hpp"
// #include "appendages/appendage_factory.hpp"
#include <rip/components/roboclaw.hpp>
#include <memory>
#include <rip/components/embed_commands.hpp>
#include <EmbMessenger/EmbMessenger.hpp>
// #include <json.hpp>
// #include <units/units.hpp>
/**
 * This is the component that is included, not what will be uploaded to the arduino.
 * Will call the embed commands.
 */
static bool m_registered = false;
namespace rip::components
{
    class RoboclawArduino : public Roboclaw
    {
    public:
        RoboclawArduino(std::shared_ptr<emb::host::EmbMessenger> m, uint8_t address = 0x80);
        
        RoboclawArduino(const std::string& name, const nlohmann::json& config, 
            std::shared_ptr<emb::host::EmbMessenger> emb, 
            std::shared_ptr<std::unordered_map<std::string, 
            std::shared_ptr<RobotComponent> > > comps = nullptr);
        
        std::vector< std::string > diagnosticCommands() const override;

        ~RoboclawArduino();

        // Rclaw lib commands
        /*
        bool ForwardM1( uint8_t speed) override;

        bool BackwardM1( uint8_t speed) override;

        bool SetMinVoltageMainBattery( uint8_t voltage) override;

        bool SetMaxVoltageMainBattery( uint8_t voltage) override;

        bool ForwardM2( uint8_t speed) override;

        bool BackwardM2( uint8_t speed) override;

        bool ForwardBackwardM1( uint8_t speed) override;

        bool ForwardBackwardM2( uint8_t speed) override;

        bool ForwardMixed( uint8_t speed) override;

        bool BackwardMixed( uint8_t speed) override;

        bool TurnRightMixed( uint8_t speed) override;

        bool TurnLeftMixed( uint8_t speed) override;

        bool ForwardBackwardMixed( uint8_t speed) override;

        bool LeftRightMixed( uint8_t speed) override;

        uint32_t ReadEncM1( uint8_t *status=NULL,bool *valid=NULL) override;

        uint32_t ReadEncM2( uint8_t *status=NULL,bool *valid=NULL) override;

        bool SetEncM1( int32_t val) override;

        bool SetEncM2( int32_t val) override;

        uint32_t ReadSpeedM1( uint8_t *status=NULL,bool *valid=NULL) override;

        uint32_t ReadSpeedM2( uint8_t *status=NULL,bool *valid=NULL) override;

        bool ResetEncoders(uint8_t address) override;

        bool ReadVersion( char *version ) override;

        uint16_t ReadMainBatteryVoltage( bool *valid=NULL) override;

        uint16_t ReadLogicBatteryVoltage( bool *valid=NULL) override;

        bool SetMinVoltageLogicBattery( uint8_t voltage) override;

        bool SetMaxVoltageLogicBattery( uint8_t voltage) override;

        bool SetM1VelocityPID( float Kp, float Ki, float Kd, uint32_t qpps) override;

        bool SetM2VelocityPID( float Kp, float Ki, float Kd, uint32_t qpps) override;

        uint32_t ReadISpeedM1( uint8_t *status=NULL,bool *valid=NULL) override;

        uint32_t ReadISpeedM2( uint8_t *status=NULL,bool *valid=NULL) override;
        */
        bool DutyM1( uint16_t duty) override;

        bool DutyM2( uint16_t duty) override;

        bool DutyM1M2( uint16_t duty1, uint16_t duty2) override;
        /*
        bool SpeedM1( uint32_t speed) override;

        bool SpeedM2( uint32_t speed) override;

        bool SpeedM1M2( uint32_t speed1, uint32_t speed2) override;

        bool SpeedAccelM1( uint32_t accel, uint32_t speed) override;

        bool SpeedAccelM2( uint32_t accel, uint32_t speed) override;

        bool SpeedAccelM1M2( uint32_t accel, uint32_t speed1, uint32_t speed2) override;

        bool SpeedDistanceM1( uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedDistanceM2( uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedDistanceM1M2( uint32_t speed1, uint32_t distance1, uint32_t speed2, uint32_t distance2, uint8_t flag=0) override;

        bool SpeedAccelDistanceM1( uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedAccelDistanceM2( uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedAccelDistanceM1M2( uint32_t accel, uint32_t speed1, uint32_t distance1, uint32_t speed2, uint32_t distance2, uint8_t flag=0) override;

        bool ReadBuffers( uint8_t &depth1, uint8_t &depth2) override;

        bool ReadPWMs( int16_t &pwm1, int16_t &pwm2) override;

        bool ReadCurrents( int16_t &current1, int16_t &current2) override;

        bool SpeedAccelM1M2_2( uint32_t accel1, uint32_t speed1, uint32_t accel2, uint32_t speed2) override;

        bool SpeedAccelDistanceM1M2_2( uint32_t accel1, uint32_t speed1, uint32_t distance1, uint32_t accel2, uint32_t speed2, uint32_t distance2, uint8_t flag=0) override;

        bool DutyAccelM1( uint16_t duty, uint32_t accel) override;

        bool DutyAccelM2( uint16_t duty, uint32_t accel) override;

        bool DutyAccelM1M2( uint16_t duty1, uint32_t accel1, uint16_t duty2, uint32_t accel2) override;

        bool ReadM1VelocityPID( float &Kp_fp,float &Ki_fp,float &Kd_fp,uint32_t &qpps) override;

        bool ReadM2VelocityPID( float &Kp_fp,float &Ki_fp,float &Kd_fp,uint32_t &qpps) override;

        bool SetMainVoltages( uint16_t min,uint16_t max) override;

        bool SetLogicVoltages( uint16_t min,uint16_t max) override;

        bool ReadMinMaxMainVoltages( uint16_t &min,uint16_t &max) override;

        bool ReadMinMaxLogicVoltages( uint16_t &min,uint16_t &max) override;

        bool SetM1PositionPID( float kp,float ki,float kd,uint32_t kiMax,uint32_t deadzone,uint32_t min,uint32_t max) override;

        bool SetM2PositionPID( float kp,float ki,float kd,uint32_t kiMax,uint32_t deadzone,uint32_t min,uint32_t max) override;

        bool ReadM1PositionPID( float &Kp,float &Ki,float &Kd,uint32_t &KiMax,uint32_t &DeadZone,uint32_t &Min,uint32_t &Max) override;

        bool ReadM2PositionPID( float &Kp,float &Ki,float &Kd,uint32_t &KiMax,uint32_t &DeadZone,uint32_t &Min,uint32_t &Max) override;

        bool SpeedAccelDeccelPositionM1( uint32_t accel,uint32_t speed,uint32_t deccel,uint32_t position,uint8_t flag) override;

        bool SpeedAccelDeccelPositionM2( uint32_t accel,uint32_t speed,uint32_t deccel,uint32_t position,uint8_t flag) override;

        bool SpeedAccelDeccelPositionM1M2( uint32_t accel1,uint32_t speed1,uint32_t deccel1,uint32_t position1,uint32_t accel2,uint32_t speed2,uint32_t deccel2,uint32_t position2,uint8_t flag) override;

        bool SetM1DefaultAccel( uint32_t accel) override;

        bool SetM2DefaultAccel( uint32_t accel) override;

        bool SetPinFunctions( uint8_t S3mode, uint8_t S4mode, uint8_t S5mode) override;

        bool GetPinFunctions( uint8_t &S3mode, uint8_t &S4mode, uint8_t &S5mode) override;

        bool SetDeadBand( uint8_t Min, uint8_t Max) override;

        bool GetDeadBand( uint8_t &Min, uint8_t &Max) override;

        bool ReadEncoders( uint32_t &enc1,uint32_t &enc2) override;

        bool ReadISpeeds( uint32_t &ispeed1,uint32_t &ispeed2) override;

        bool RestoreDefaults(uint8_t address) override;

        bool ReadTemp( uint16_t &temp) override;

        bool ReadTemp2( uint16_t &temp) override;

        uint16_t ReadError( bool *valid=NULL) override;

        bool ReadEncoderModes( uint8_t &M1mode, uint8_t &M2mode) override;

        bool SetM1EncoderMode( uint8_t mode) override;

        bool SetM2EncoderMode( uint8_t mode) override;

        bool WriteNVM(uint8_t address) override;

        bool ReadNVM(uint8_t address) override;

        bool SetConfig( uint16_t config) override;

        bool GetConfig( uint16_t &config) override;

        bool SetM1MaxCurrent( uint32_t max) override;

        bool SetM2MaxCurrent( uint32_t max) override;

        bool ReadM1MaxCurrent( uint32_t &max) override;

        bool ReadM2MaxCurrent( uint32_t &max) override;

        bool SetPWMMode( uint8_t mode ) override;

        bool GetPWMMode( uint8_t &mode ) override;
        */
        void stop() override;
        
        // void diagnostic() override;
        
    private:
    
        void registerCommands();

    };
}
#endif  // ROBOCLAW_ARDUINO_HPP
