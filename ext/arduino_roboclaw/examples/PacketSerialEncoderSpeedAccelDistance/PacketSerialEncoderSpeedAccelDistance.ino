//See BareMinimum example for a list of library functions

//Includes required to use Roboclaw library
#include "RoboClaw.h"

//See limitations of Arduino SoftwareSerial
Serial2.begin(115200);
RoboClaw l_rclaw(&Serial2,10000);
RoboClaw r_rclaw(&Serial2,10000);

#define l_address 0x80
#define r_address 0x81

//Velocity PID coefficients
//#define Kp 1.0
//#define Ki 0.5
//#define Kd 0.25
#define qpps 3591.8

void setup()
{
  //Open Serial and roboclaw serial ports
  Serial1.begin(57600);
  l_rclaw.begin(115200);
  r_rclaw.begin(115200);
  //Set PID Coefficients
  //roboclaw.SetM1VelocityPID(address,Kd,Kp,Ki,qpps);
  //roboclaw.SetM2VelocityPID(address,Kd,Kp,Ki,qpps);
}

void displayspeed(void)
{
  uint8_t l_status1,l_status2,l_status3,l_status4;
  uint8_t r_status1,r_status2,r_status3,r_status4;

  bool l_valid1,l_valid2,l_valid3,l_valid4;
  bool r_valid1,r_valid2,r_valid3,r_valid4;

  int32_t l_enc1= roboclawLeft.ReadEncM1(address, &status1, &valid1);
  int32_t l_enc2 = roboclawLeft.ReadEncM2(address, &status2, &valid2);
  int32_t r_enc1= roboclawRight.ReadEncM1(address, &status1, &valid1);
  int32_t r_enc2 = roboclawRight.ReadEncM2(address, &status2, &valid2);

  int32_t l_speed1 = roboclawLeft.ReadSpeedM1(address, &status3, &valid3);
  int32_t l_speed2 = roboclawLeft.ReadSpeedM2(address, &status4, &valid4);
  int32_t r_speed1 = roboclawRight.ReadSpeedM1(address, &status3, &valid3);
  int32_t r_speed2 = roboclawRight.ReadSpeedM2(address, &status4, &valid4);

  Serial.print("l_ Encoder1:");
  if(l_valid1)
  {
    Serial.print(l_enc1,DEC);
    Serial.print(" ");
    Serial.print(l_status1,HEX);
    Serial.print(" ");
  }
  else{
	Serial.print("failed ");
  }
  Serial.print("l Encoder2:");
  if(l_valid2){
    Serial.print(l_enc2,DEC);
    Serial.print(" ");
    Serial.print(l_status2,HEX);
    Serial.print(" ");
  }
  else{
	Serial.print("failed ");
  }
  Serial.print("l Speed1:");
  if(l_valid3){
    Serial.print(l_speed1,DEC);
    Serial.print(" ");
  }
  else{
	Serial.print("l failed ");
  }
  Serial.print("l Speed2:");
  if(l_valid4){
    Serial.print(l_speed2,DEC);
    Serial.print(" ");
  }
  else{
	Serial.print("l failed ");
  }
  Serial.println();

  Serial.print("r Encoder1:");
  if(r_valid1)
  {
    Serial.print(r_enc1,DEC);
    Serial.print(" ");
    Serial.print(r_status1,HEX);
    Serial.print(" ");
  }
  else{
   Serial.print("r failed ");
  }
  Serial.print("r Encoder2:");
  if(r_valid2){
    Serial.print(r_enc2,DEC);
    Serial.print(" ");
    Serial.print(r_status2,HEX);
    Serial.print(" ");
  }
  else{
   Serial.print("r failed ");
  }
  Serial.print("r Speed1:");
  if(r_valid3){
    Serial.print(r_speed1,DEC);
    Serial.print(" ");
  }
  else{
   Serial.print("failed ");
  }
  Serial.print("Speed2:");
  if(r_valid4){
    Serial.print(r_speed2,DEC);
    Serial.print(" ");
  }
  else{
   Serial.print("failed ");
  }
  Serial.println();
}

void loop() {
  uint8_t l_depth1,l_depth2, r_depth1, r_depth2;

  l_roboclaw.SpeedAccelDistanceM1(address,12000,12000,42000,1);
  l_roboclaw.SpeedAccelDistanceM2(address,12000,-12000,42000,1);
  l_roboclaw.SpeedAccelDistanceM1(address,12000,0,0);  //distance traveled is v*v/2a = 12000*12000/2*12000 = 6000
  l_roboclaw.SpeedAccelDistanceM2(address,12000,0,0);  //that makes the total move in ondirection 48000

  r_roboclaw.SpeedAccelDistanceM1(address,12000,12000,42000,1);
  r_roboclaw.SpeedAccelDistanceM2(address,12000,-12000,42000,1);
  r_roboclaw.SpeedAccelDistanceM1(address,12000,0,0);  //distance traveled is v*v/2a = 12000*12000/2*12000 = 6000
  r_roboclaw.SpeedAccelDistanceM2(address,12000,0,0);  //that makes the total move in ondirection 48000
  do{
    displayspeed();
    l_roboclaw.ReadBuffers(address,l_depth1,l_depth2);
    r_roboclaw.ReadBuffers(address,r_depth1,r_depth2);

}while(l_depth1!=0x80 && l_depth2!=0x80 && r_depth1!=0x80 && r_depth2!=0x80);	//loop until distance command completes

  delay(1000);

  l_roboclaw.SpeedAccelDistanceM1(address,12000,-12000,42000,1);
  l_roboclaw.SpeedAccelDistanceM2(address,12000,12000,42000,1);
  l_roboclaw.SpeedAccelDistanceM1(address,12000,0,0);
  l_roboclaw.SpeedAccelDistanceM2(address,12000,0,0);

  r_roboclaw.SpeedAccelDistanceM1(address,12000,-12000,42000,1);
  r_roboclaw.SpeedAccelDistanceM2(address,12000,12000,42000,1);
  r_roboclaw.SpeedAccelDistanceM1(address,12000,0,0);
  r_roboclaw.SpeedAccelDistanceM2(address,12000,0,0);
  do{
    displayspeed();
    l_roboclaw.ReadBuffers(address,l_depth1,l_depth2);
    r_roboclaw.ReadBuffers(address,r_depth1,r_depth2);

}while(l_depth1!=0x80 && l_depth2!=0x80 r_depth1!=0x80 && r_depth2!=0x80);	//loop until distance command completes

  delay(1000);

}
