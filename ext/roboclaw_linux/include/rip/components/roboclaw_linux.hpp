#ifndef ROBOCLAW_LINUX_HPP 
#define ROBOCLAW_LINUX_HPP

#include <stdarg.h>

#include <inttypes.h>
#include <unistd.h>
#include <string>
#include <termios.h>
#include "rip/components/roboclaw.hpp"
/******************************************************************************
* Definitions
******************************************************************************/

#define _RC_VERSION 10 // software version of this library
#ifndef GCC_VERSION
#define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#endif

#define _SS_VERSION 16
namespace rip::components
{
    class RoboclawLinux : public Roboclaw
    {	
        enum {M1FORWARD = 0,
            M1BACKWARD = 1,
            SETMINMB = 2,
            SETMAXMB = 3,
            M2FORWARD = 4,
            M2BACKWARD = 5,
            M17BIT = 6,
            M27BIT = 7,
            MIXEDFORWARD = 8,
            MIXEDBACKWARD = 9,
            MIXEDRIGHT = 10,
            MIXEDLEFT = 11,
            MIXEDFB = 12,
            MIXEDLR = 13,
            GETM1ENC = 16,
            GETM2ENC = 17,
            GETM1SPEED = 18,
            GETM2SPEED = 19,
            RESETENC = 20,
            GETVERSION = 21,
            SETM1ENCCOUNT = 22,
            SETM2ENCCOUNT = 23,
            GETMBATT = 24,
            GETLBATT = 25,
            SETMINLB = 26,
            SETMAXLB = 27,
            SETM1PID = 28,
            SETM2PID = 29,
            GETM1ISPEED = 30,
            GETM2ISPEED = 31,
            M1DUTY = 32,
            M2DUTY = 33,
            MIXEDDUTY = 34,
            M1SPEED = 35,
            M2SPEED = 36,
            MIXEDSPEED = 37,
            M1SPEEDACCEL = 38,
            M2SPEEDACCEL = 39,
            MIXEDSPEEDACCEL = 40,
            M1SPEEDDIST = 41,
            M2SPEEDDIST = 42,
            MIXEDSPEEDDIST = 43,
            M1SPEEDACCELDIST = 44,
            M2SPEEDACCELDIST = 45,
            MIXEDSPEEDACCELDIST = 46,
            GETBUFFERS = 47,
            GETPWMS = 48,
            GETCURRENTS = 49,
            MIXEDSPEED2ACCEL = 50,
            MIXEDSPEED2ACCELDIST = 51,
            M1DUTYACCEL = 52,
            M2DUTYACCEL = 53,
            MIXEDDUTYACCEL = 54,
            READM1PID = 55,
            READM2PID = 56,
            SETMAINVOLTAGES = 57,
            SETLOGICVOLTAGES = 58,
            GETMINMAXMAINVOLTAGES = 59,
            GETMINMAXLOGICVOLTAGES = 60,
            SETM1POSPID = 61,
            SETM2POSPID = 62,
            READM1POSPID = 63,
            READM2POSPID = 64,
            M1SPEEDACCELDECCELPOS = 65,
            M2SPEEDACCELDECCELPOS = 66,
            MIXEDSPEEDACCELDECCELPOS = 67,
            SETM1DEFAULTACCEL = 68,
            SETM2DEFAULTACCEL = 69,
            SETPINFUNCTIONS = 74,
            GETPINFUNCTIONS = 75,
            SETDEADBAND	= 76,
            GETDEADBAND	= 77,
            GETENCODERS = 78,
            GETISPEEDS = 79,
            RESTOREDEFAULTS = 80,
            GETTEMP = 82,
            GETTEMP2 = 83,	//Only valid on some models
            GETERROR = 90,
            GETENCODERMODE = 91,
            SETM1ENCODERMODE = 92,
            SETM2ENCODERMODE = 93,
            WRITENVM = 94,
            READNVM = 95,	//Reloads values from Flash into Ram
            SETCONFIG = 98,
            GETCONFIG = 99,
            SETM1MAXCURRENT = 133,
            SETM2MAXCURRENT = 134,
            GETM1MAXCURRENT = 135,
            GETM2MAXCURRENT = 136,
            SETPWMMODE = 148,
            GETPWMMODE = 149,
            FLAGBOOTLOADER = 255};	//Only available via USB communications
    public:
        // public methods
        RoboclawLinux();

        RoboclawLinux( const std::string& a_portName, uint8_t a_timeout, uint8_t a_address );

        ~RoboclawLinux();

        std::vector< std::string > diagnosticCommands() const override;

        void stop() override;

        bool ForwardM1( uint8_t speed) override;

        bool BackwardM1( uint8_t speed) override;

        bool SetMinVoltageMainBattery( uint8_t voltage) override;

        bool SetMaxVoltageMainBattery( uint8_t voltage) override;

        bool ForwardM2( uint8_t speed) override;

        bool BackwardM2( uint8_t speed) override;

        bool ForwardBackwardM1( uint8_t speed) override;

        bool ForwardBackwardM2( uint8_t speed) override;

        bool ForwardMixed( uint8_t speed) override;

        bool BackwardMixed( uint8_t speed) override;

        bool TurnRightMixed( uint8_t speed) override;

        bool TurnLeftMixed( uint8_t speed) override;

        bool ForwardBackwardMixed( uint8_t speed) override;

        bool LeftRightMixed( uint8_t speed) override;

        uint32_t ReadEncM1( uint8_t *status=NULL,bool *valid=NULL) override;

        uint32_t ReadEncM2( uint8_t *status=NULL,bool *valid=NULL) override;

        bool SetEncM1( int32_t val) override;

        bool SetEncM2( int32_t val) override;

        uint32_t ReadSpeedM1( uint8_t *status=NULL,bool *valid=NULL) override;

        uint32_t ReadSpeedM2( uint8_t *status=NULL,bool *valid=NULL) override;

        bool ResetEncoders(uint8_t address) override;

        bool ReadVersion( char *version ) override;

        uint16_t ReadMainBatteryVoltage( bool *valid=NULL) override;

        uint16_t ReadLogicBatteryVoltage( bool *valid=NULL) override;

        bool SetMinVoltageLogicBattery( uint8_t voltage) override;

        bool SetMaxVoltageLogicBattery( uint8_t voltage) override;

        bool SetM1VelocityPID( float Kp, float Ki, float Kd, uint32_t qpps) override;

        bool SetM2VelocityPID( float Kp, float Ki, float Kd, uint32_t qpps) override;

        uint32_t ReadISpeedM1( uint8_t *status=NULL,bool *valid=NULL) override;

        uint32_t ReadISpeedM2( uint8_t *status=NULL,bool *valid=NULL) override;

        bool DutyM1( uint16_t duty) override;

        bool DutyM2( uint16_t duty) override;

        bool DutyM1M2( uint16_t duty1, uint16_t duty2) override;

        bool SpeedM1( uint32_t speed) override;

        bool SpeedM2( uint32_t speed) override;

        bool SpeedM1M2( uint32_t speed1, uint32_t speed2) override;

        bool SpeedAccelM1( uint32_t accel, uint32_t speed) override;

        bool SpeedAccelM2( uint32_t accel, uint32_t speed) override;

        bool SpeedAccelM1M2( uint32_t accel, uint32_t speed1, uint32_t speed2) override;

        bool SpeedDistanceM1( uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedDistanceM2( uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedDistanceM1M2( uint32_t speed1, uint32_t distance1, uint32_t speed2, uint32_t distance2, uint8_t flag=0) override;

        bool SpeedAccelDistanceM1( uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedAccelDistanceM2( uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag=0) override;

        bool SpeedAccelDistanceM1M2( uint32_t accel, uint32_t speed1, uint32_t distance1, uint32_t speed2, uint32_t distance2, uint8_t flag=0) override;

        bool ReadBuffers( uint8_t &depth1, uint8_t &depth2) override;

        bool ReadPWMs( int16_t &pwm1, int16_t &pwm2) override;

        bool ReadCurrents( int16_t &current1, int16_t &current2) override;

        bool SpeedAccelM1M2_2( uint32_t accel1, uint32_t speed1, uint32_t accel2, uint32_t speed2) override;

        bool SpeedAccelDistanceM1M2_2( uint32_t accel1, uint32_t speed1, uint32_t distance1, uint32_t accel2, uint32_t speed2, uint32_t distance2, uint8_t flag=0) override;

        bool DutyAccelM1( uint16_t duty, uint32_t accel) override;

        bool DutyAccelM2( uint16_t duty, uint32_t accel) override;

        bool DutyAccelM1M2( uint16_t duty1, uint32_t accel1, uint16_t duty2, uint32_t accel2) override;

        bool ReadM1VelocityPID( float &Kp_fp,float &Ki_fp,float &Kd_fp,uint32_t &qpps) override;

        bool ReadM2VelocityPID( float &Kp_fp,float &Ki_fp,float &Kd_fp,uint32_t &qpps) override;

        bool SetMainVoltages( uint16_t min,uint16_t max) override;

        bool SetLogicVoltages( uint16_t min,uint16_t max) override;

        bool ReadMinMaxMainVoltages( uint16_t &min,uint16_t &max) override;

        bool ReadMinMaxLogicVoltages( uint16_t &min,uint16_t &max) override;

        bool SetM1PositionPID( float kp,float ki,float kd,uint32_t kiMax,uint32_t deadzone,uint32_t min,uint32_t max) override;

        bool SetM2PositionPID( float kp,float ki,float kd,uint32_t kiMax,uint32_t deadzone,uint32_t min,uint32_t max) override;

        bool ReadM1PositionPID( float &Kp,float &Ki,float &Kd,uint32_t &KiMax,uint32_t &DeadZone,uint32_t &Min,uint32_t &Max) override;

        bool ReadM2PositionPID( float &Kp,float &Ki,float &Kd,uint32_t &KiMax,uint32_t &DeadZone,uint32_t &Min,uint32_t &Max) override;

        bool SpeedAccelDeccelPositionM1( uint32_t accel,uint32_t speed,uint32_t deccel,uint32_t position,uint8_t flag) override;

        bool SpeedAccelDeccelPositionM2( uint32_t accel,uint32_t speed,uint32_t deccel,uint32_t position,uint8_t flag) override;

        bool SpeedAccelDeccelPositionM1M2( uint32_t accel1,uint32_t speed1,uint32_t deccel1,uint32_t position1,uint32_t accel2,uint32_t speed2,uint32_t deccel2,uint32_t position2,uint8_t flag) override;

        bool SetM1DefaultAccel( uint32_t accel) override;

        bool SetM2DefaultAccel( uint32_t accel) override;

        bool SetPinFunctions( uint8_t S3mode, uint8_t S4mode, uint8_t S5mode) override;

        bool GetPinFunctions( uint8_t &S3mode, uint8_t &S4mode, uint8_t &S5mode) override;

        bool SetDeadBand( uint8_t Min, uint8_t Max) override;

        bool GetDeadBand( uint8_t &Min, uint8_t &Max) override;

        bool ReadEncoders( uint32_t &enc1,uint32_t &enc2) override;

        bool ReadISpeeds( uint32_t &ispeed1,uint32_t &ispeed2) override;

        bool RestoreDefaults(uint8_t address) override;

        bool ReadTemp( uint16_t &temp) override;

        bool ReadTemp2( uint16_t &temp) override;

        uint16_t ReadError( bool *valid=NULL) override;

        bool ReadEncoderModes( uint8_t &M1mode, uint8_t &M2mode) override;

        bool SetM1EncoderMode( uint8_t mode) override;

        bool SetM2EncoderMode( uint8_t mode) override;

        bool WriteNVM(uint8_t address) override;

        bool ReadNVM(uint8_t address) override;

        bool SetConfig( uint16_t config) override;

        bool GetConfig( uint16_t &config) override;

        bool SetM1MaxCurrent( uint32_t max) override;

        bool SetM2MaxCurrent( uint32_t max) override;

        bool ReadM1MaxCurrent( uint32_t &max) override;

        bool ReadM2MaxCurrent( uint32_t &max) override;

        bool SetPWMMode( uint8_t mode ) override;

        bool GetPWMMode( uint8_t &mode ) override;

        static int16_t library_version() 
        { 
            return _SS_VERSION;
        }

        int available();

        void begin(long speed);

        void end();

        //	int peek();

        uint8_t read();

        uint8_t read(uint32_t timeout);

        bool listen();

        virtual size_t write(uint8_t byte);

        virtual void flush();

        void clear();


    protected:
        void init();

        void crc_clear();

        void crc_update ( uint8_t data );

        uint16_t crc_get();

        bool write_n(uint8_t byte, ... );

        bool read_n( uint8_t address, uint8_t byte, uint8_t cmd,...);

        uint32_t Read4_1( uint8_t address, uint8_t cmd, uint8_t *status, bool *valid);

        uint32_t Read4( uint8_t address, uint8_t cmd,bool *valid);

        uint16_t Read2( uint8_t address, uint8_t cmd, bool *valid);

        uint8_t Read1( uint8_t address,  uint8_t cmd, bool *valid);


    private:
        std::string m_portName;

        uint16_t m_crc;
        uint32_t m_timeout;
        uint8_t  m_address;
        int      m_serial;
    };
}
#endif
