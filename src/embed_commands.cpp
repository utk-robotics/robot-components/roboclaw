#include <rip/components/embed_commands.hpp>

namespace commands 
{
    /*
    ForwardM1::ForwardM1(uint8_t speed)
        : m_speed(speed)
    {}

    void ForwardM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void ForwardM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    BackwardM1::BackwardM1(uint8_t speed)
        : m_speed(speed)
    {}

    void BackwardM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void BackwardM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetMinVoltageMainBattery::SetMinVoltageMainBattery(uint8_t voltage)
        : m_voltage(voltage)
    {}

    void SetMinVoltageMainBattery::send(EmbMessenger* messenger)
    {
        messenger->write(m_voltage);
    }

    void SetMinVoltageMainBattery::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetMaxVoltageMainBattery::SetMaxVoltageMainBattery(uint8_t voltage)
        : m_voltage(voltage)
    {}

    void SetMaxVoltageMainBattery::send(EmbMessenger* messenger)
    {
        messenger->write(m_voltage);
    }

    void SetMaxVoltageMainBattery::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ForwardM2::ForwardM2(uint8_t speed)
        : m_speed(speed)
    {}

    void ForwardM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void ForwardM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    BackwardM2::BackwardM2(uint8_t speed)
        : m_speed(speed)
    {}

    void BackwardM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void BackwardM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ForwardBackwardM1::ForwardBackwardM1(uint8_t speed)
        : m_speed(speed)
    {}

    void ForwardBackwardM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void ForwardBackwardM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ForwardBackwardM2::ForwardBackwardM2(uint8_t speed)
        : m_speed(speed)
    {}

    void ForwardBackwardM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void ForwardBackwardM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ForwardMixed::ForwardMixed(uint8_t speed)
        : m_speed(speed)
    {}

    void ForwardMixed::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void ForwardMixed::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    BackwardMixed::BackwardMixed(uint8_t speed)
        : m_speed(speed)
    {}

    void BackwardMixed::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void BackwardMixed::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    TurnRightMixed::TurnRightMixed(uint8_t speed)
        : m_speed(speed)
    {}

    void TurnRightMixed::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void TurnRightMixed::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    TurnLeftMixed::TurnLeftMixed(uint8_t speed)
        : m_speed(speed)
    {}

    void TurnLeftMixed::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void TurnLeftMixed::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ForwardBackwardMixed::ForwardBackwardMixed(uint8_t speed)
        : m_speed(speed)
    {}

    void ForwardBackwardMixed::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void ForwardBackwardMixed::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    LeftRightMixed::LeftRightMixed(uint8_t speed)
        : m_speed(speed)
    {}

    void LeftRightMixed::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void LeftRightMixed::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadEncM1::ReadEncM1(bool* valid)
        : m_valid(valid)
    {}

    void ReadEncM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadEncM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadEncM2::ReadEncM2(bool* valid)
        : m_valid(valid)
    {}

    void ReadEncM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadEncM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetEncM1::SetEncM1(int32_t val)
        : m_val(val)
    {}

    void SetEncM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_val);
    }

    void SetEncM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetEncM2::SetEncM2(int32_t val)
        : m_val(val)
    {}

    void SetEncM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_val);
    }

    void SetEncM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadSpeedM1::ReadSpeedM1(bool* valid)
        : m_valid(valid)
    {}

    void ReadSpeedM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadSpeedM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadSpeedM2::ReadSpeedM2(bool* valid)
        : m_valid(valid)
    {}

    void ReadSpeedM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadSpeedM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ResetEncoders::ResetEncoders(uint8_t address)
        : m_address(address)
    {}

    void ResetEncoders::send(EmbMessenger* messenger)
    {
        messenger->write(m_address);
    }

    void ResetEncoders::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadVersion::ReadVersion(char* version)
        : m_version(version)
    {}

    void ReadVersion::send(EmbMessenger* messenger)
    {
        messenger->write(m_version);
    }

    void ReadVersion::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadMainBatteryVoltage::ReadMainBatteryVoltage(bool* valid)
        : m_valid(valid)
    {}

    void ReadMainBatteryVoltage::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadMainBatteryVoltage::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadLogicBatteryVoltage::ReadLogicBatteryVoltage(bool* valid)
        : m_valid(valid)
    {}

    void ReadLogicBatteryVoltage::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadLogicBatteryVoltage::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetMinVoltageLogicBattery::SetMinVoltageLogicBattery(uint8_t voltage)
        : m_voltage(voltage)
    {}

    void SetMinVoltageLogicBattery::send(EmbMessenger* messenger)
    {
        messenger->write(m_voltage);
    }

    void SetMinVoltageLogicBattery::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetMaxVoltageLogicBattery::SetMaxVoltageLogicBattery(uint8_t voltage)
        : m_voltage(voltage)
    {}

    void SetMaxVoltageLogicBattery::send(EmbMessenger* messenger)
    {
        messenger->write(m_voltage);
    }

    void SetMaxVoltageLogicBattery::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM1VelocityPID::SetM1VelocityPID(float Kp, float Ki, uint32_t qpps)
        : m_Kp(Kp)
        , m_Ki(Ki)
        , m_qpps(qpps)
    {}

    void SetM1VelocityPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_Kp, m_Ki, m_qpps);
    }

    void SetM1VelocityPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM2VelocityPID::SetM2VelocityPID(float Kp, float Ki, uint32_t qpps)
        : m_Kp(Kp)
        , m_Ki(Ki)
        , m_qpps(qpps)
    {}

    void SetM2VelocityPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_Kp, m_Ki, m_qpps);
    }

    void SetM2VelocityPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadISpeedM1::ReadISpeedM1(bool* valid)
        : m_valid(valid)
    {}

    void ReadISpeedM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadISpeedM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadISpeedM2::ReadISpeedM2(bool* valid)
        : m_valid(valid)
    {}

    void ReadISpeedM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadISpeedM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }
    */
    DutyM1::DutyM1(uint8_t address, uint16_t duty)
        : m_address(address), m_duty(duty)
    {}

    void DutyM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_address);
        messenger->write(m_duty);
    }

    DutyM2::DutyM2(uint8_t address, uint16_t duty)
        : m_address(address)
        , m_duty(duty)
    {}

    void DutyM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_address);
        messenger->write(m_duty);
    }

    DutyM1M2::DutyM1M2(uint8_t address, uint16_t duty1, uint16_t duty2)
        : m_address(address), m_duty1(duty1), m_duty2(duty2)
    {}

    void DutyM1M2::send(EmbMessenger* messenger)
    {
        messenger->write(m_address);
        messenger->write(m_duty1);
        messenger->write(m_duty2);
    }

    /*
    SpeedM1::SpeedM1(uint32_t speed)
        : m_speed(speed)
    {}

    void SpeedM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void SpeedM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedM2::SpeedM2(uint32_t speed)
        : m_speed(speed)
    {}

    void SpeedM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void SpeedM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedM1M2::SpeedM1M2(uint32_t speed2)
        : m_speed2(speed2)
    {}

    void SpeedM1M2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed2);
    }

    void SpeedM1M2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelM1::SpeedAccelM1(uint32_t speed)
        : m_speed(speed)
    {}

    void SpeedAccelM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void SpeedAccelM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelM2::SpeedAccelM2(uint32_t speed)
        : m_speed(speed)
    {}

    void SpeedAccelM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed);
    }

    void SpeedAccelM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelM1M2::SpeedAccelM1M2(uint32_t accel, uint32_t speed2)
        : m_accel(accel)
        , m_speed2(speed2)
    {}

    void SpeedAccelM1M2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel, m_speed2);
    }

    void SpeedAccelM1M2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedDistanceM1::SpeedDistanceM1(uint32_t speed, uint8_t flag)
        : m_speed(speed)
        , m_flag(flag)
    {}

    void SpeedDistanceM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed, m_flag);
    }

    void SpeedDistanceM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedDistanceM2::SpeedDistanceM2(uint32_t speed, uint8_t flag)
        : m_speed(speed)
        , m_flag(flag)
    {}

    void SpeedDistanceM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed, m_flag);
    }

    void SpeedDistanceM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedDistanceM1M2::SpeedDistanceM1M2(uint32_t speed1, uint32_t distance1, uint32_t speed2, uint8_t flag)
        : m_speed1(speed1)
        , m_distance1(distance1)
        , m_speed2(speed2)
        , m_flag(flag)
    {}

    void SpeedDistanceM1M2::send(EmbMessenger* messenger)
    {
        messenger->write(m_speed1, m_distance1, m_speed2, m_flag);
    }

    void SpeedDistanceM1M2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelDistanceM1::SpeedAccelDistanceM1(uint32_t accel, uint32_t speed, uint8_t flag)
        : m_accel(accel)
        , m_speed(speed)
        , m_flag(flag)
    {}

    void SpeedAccelDistanceM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel, m_speed, m_flag);
    }

    void SpeedAccelDistanceM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelDistanceM2::SpeedAccelDistanceM2(uint32_t accel, uint32_t speed, uint8_t flag)
        : m_accel(accel)
        , m_speed(speed)
        , m_flag(flag)
    {}

    void SpeedAccelDistanceM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel, m_speed, m_flag);
    }

    void SpeedAccelDistanceM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelDistanceM1M2::SpeedAccelDistanceM1M2(uint32_t accel,
                                                uint32_t speed1,
                                                uint32_t distance1,
                                                uint32_t speed2,
                                                uint8_t flag)
        : m_accel(accel)
        , m_speed1(speed1)
        , m_distance1(distance1)
        , m_speed2(speed2)
        , m_flag(flag)
    {}

    void SpeedAccelDistanceM1M2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel, m_speed1, m_distance1, m_speed2, m_flag);
    }

    void SpeedAccelDistanceM1M2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadBuffers::ReadBuffers(uint8_t depth2)
        : m_depth2(depth2)
    {}

    void ReadBuffers::send(EmbMessenger* messenger)
    {
        messenger->write(m_depth2);
    }

    void ReadBuffers::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadPWMs::ReadPWMs(int16_t pwm2)
        : m_pwm2(pwm2)
    {}

    void ReadPWMs::send(EmbMessenger* messenger)
    {
        messenger->write(m_pwm2);
    }

    void ReadPWMs::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadCurrents::ReadCurrents(int16_t current2)
        : m_current2(current2)
    {}

    void ReadCurrents::send(EmbMessenger* messenger)
    {
        messenger->write(m_current2);
    }

    void ReadCurrents::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelM1M2_2::SpeedAccelM1M2_2(uint32_t accel1, uint32_t speed1, uint32_t speed2)
        : m_accel1(accel1)
        , m_speed1(speed1)
        , m_speed2(speed2)
    {}

    void SpeedAccelM1M2_2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel1, m_speed1, m_speed2);
    }

    void SpeedAccelM1M2_2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelDistanceM1M2_2::SpeedAccelDistanceM1M2_2(uint32_t accel1,
                                                    uint32_t speed1,
                                                    uint32_t distance1,
                                                    uint32_t accel2,
                                                    uint32_t speed2,
                                                    uint8_t flag)
        : m_accel1(accel1)
        , m_speed1(speed1)
        , m_distance1(distance1)
        , m_accel2(accel2)
        , m_speed2(speed2)
        , m_flag(flag)
    {}

    void SpeedAccelDistanceM1M2_2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel1, m_speed1, m_distance1, m_accel2, m_speed2, m_flag);
    }

    void SpeedAccelDistanceM1M2_2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    DutyAccelM1::DutyAccelM1(uint32_t accel)
        : m_accel(accel)
    {}

    void DutyAccelM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel);
    }

    void DutyAccelM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    DutyAccelM2::DutyAccelM2(uint32_t accel)
        : m_accel(accel)
    {}

    void DutyAccelM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel);
    }

    void DutyAccelM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    DutyAccelM1M2::DutyAccelM1M2(uint16_t duty1, uint32_t accel1, uint32_t accel2)
        : m_duty1(duty1)
        , m_accel1(accel1)
        , m_accel2(accel2)
    {}

    void DutyAccelM1M2::send(EmbMessenger* messenger)
    {
        messenger->write(m_duty1, m_accel1, m_accel2);
    }

    void DutyAccelM1M2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadM1VelocityPID::ReadM1VelocityPID(float Kp_fp, float Ki_fp, uint32_t qpps)
        : m_Kp_fp(Kp_fp)
        , m_Ki_fp(Ki_fp)
        , m_qpps(qpps)
    {}

    void ReadM1VelocityPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_Kp_fp, m_Ki_fp, m_qpps);
    }

    void ReadM1VelocityPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadM2VelocityPID::ReadM2VelocityPID(float Kp_fp, float Ki_fp, uint32_t qpps)
        : m_Kp_fp(Kp_fp)
        , m_Ki_fp(Ki_fp)
        , m_qpps(qpps)
    {}

    void ReadM2VelocityPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_Kp_fp, m_Ki_fp, m_qpps);
    }

    void ReadM2VelocityPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetMainVoltages::SetMainVoltages(uint16_t max)
        : m_max(max)
    {}

    void SetMainVoltages::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void SetMainVoltages::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetLogicVoltages::SetLogicVoltages(uint16_t max)
        : m_max(max)
    {}

    void SetLogicVoltages::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void SetLogicVoltages::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadMinMaxMainVoltages::ReadMinMaxMainVoltages(uint16_t max)
        : m_max(max)
    {}

    void ReadMinMaxMainVoltages::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void ReadMinMaxMainVoltages::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadMinMaxLogicVoltages::ReadMinMaxLogicVoltages(uint16_t max)
        : m_max(max)
    {}

    void ReadMinMaxLogicVoltages::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void ReadMinMaxLogicVoltages::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM1PositionPID::SetM1PositionPID(float kp, float ki, float kd, uint32_t kiMax, uint32_t deadzone, uint32_t max)
        : m_kp(kp)
        , m_ki(ki)
        , m_kd(kd)
        , m_kiMax(kiMax)
        , m_deadzone(deadzone)
        , m_max(max)
    {}

    void SetM1PositionPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_kp, m_ki, m_kd, m_kiMax, m_deadzone, m_max);
    }

    void SetM1PositionPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM2PositionPID::SetM2PositionPID(float kp, float ki, float kd, uint32_t kiMax, uint32_t deadzone, uint32_t max)
        : m_kp(kp)
        , m_ki(ki)
        , m_kd(kd)
        , m_kiMax(kiMax)
        , m_deadzone(deadzone)
        , m_max(max)
    {}

    void SetM2PositionPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_kp, m_ki, m_kd, m_kiMax, m_deadzone, m_max);
    }

    void SetM2PositionPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadM1PositionPID::ReadM1PositionPID(float Kp,
                                        float Ki,
                                        float Kd,
                                        uint32_t KiMax,
                                        uint32_t DeadZone,
                                        uint32_t Max)
        : m_Kp(Kp)
        , m_Ki(Ki)
        , m_Kd(Kd)
        , m_KiMax(KiMax)
        , m_DeadZone(DeadZone)
        , m_Max(Max)
    {}

    void ReadM1PositionPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_Kp, m_Ki, m_Kd, m_KiMax, m_DeadZone, m_Max);
    }

    void ReadM1PositionPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadM2PositionPID::ReadM2PositionPID(float Kp,
                                        float Ki,
                                        float Kd,
                                        uint32_t KiMax,
                                        uint32_t DeadZone,
                                        uint32_t Max)
        : m_Kp(Kp)
        , m_Ki(Ki)
        , m_Kd(Kd)
        , m_KiMax(KiMax)
        , m_DeadZone(DeadZone)
        , m_Max(Max)
    {}

    void ReadM2PositionPID::send(EmbMessenger* messenger)
    {
        messenger->write(m_Kp, m_Ki, m_Kd, m_KiMax, m_DeadZone, m_Max);
    }

    void ReadM2PositionPID::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelDeccelPositionM1::SpeedAccelDeccelPositionM1(uint32_t accel, uint32_t speed, uint32_t deccel, uint8_t flag)
        : m_accel(accel)
        , m_speed(speed)
        , m_deccel(deccel)
        , m_flag(flag)
    {}

    void SpeedAccelDeccelPositionM1::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel, m_speed, m_deccel, m_flag);
    }

    void SpeedAccelDeccelPositionM1::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelDeccelPositionM2::SpeedAccelDeccelPositionM2(uint32_t accel, uint32_t speed, uint32_t deccel, uint8_t flag)
        : m_accel(accel)
        , m_speed(speed)
        , m_deccel(deccel)
        , m_flag(flag)
    {}

    void SpeedAccelDeccelPositionM2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel, m_speed, m_deccel, m_flag);
    }

    void SpeedAccelDeccelPositionM2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SpeedAccelDeccelPositionM1M2::SpeedAccelDeccelPositionM1M2(uint32_t accel1,
                                                            uint32_t speed1,
                                                            uint32_t deccel1,
                                                            uint32_t position1,
                                                            uint32_t accel2,
                                                            uint32_t speed2,
                                                            uint32_t deccel2,
                                                            uint8_t flag)
        : m_accel1(accel1)
        , m_speed1(speed1)
        , m_deccel1(deccel1)
        , m_position1(position1)
        , m_accel2(accel2)
        , m_speed2(speed2)
        , m_deccel2(deccel2)
        , m_flag(flag)
    {}

    void SpeedAccelDeccelPositionM1M2::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel1, m_speed1, m_deccel1, m_position1, m_accel2, m_speed2, m_deccel2, m_flag);
    }

    void SpeedAccelDeccelPositionM1M2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM1DefaultAccel::SetM1DefaultAccel(uint32_t accel)
        : m_accel(accel)
    {}

    void SetM1DefaultAccel::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel);
    }

    void SetM1DefaultAccel::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM2DefaultAccel::SetM2DefaultAccel(uint32_t accel)
        : m_accel(accel)
    {}

    void SetM2DefaultAccel::send(EmbMessenger* messenger)
    {
        messenger->write(m_accel);
    }

    void SetM2DefaultAccel::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetPinFunctions::SetPinFunctions(uint8_t S3mode, uint8_t S5mode)
        : m_S3mode(S3mode)
        , m_S5mode(S5mode)
    {}

    void SetPinFunctions::send(EmbMessenger* messenger)
    {
        messenger->write(m_S3mode, m_S5mode);
    }

    void SetPinFunctions::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    GetPinFunctions::GetPinFunctions(uint8_t S3mode, uint8_t S5mode)
        : m_S3mode(S3mode)
        , m_S5mode(S5mode)
    {}

    void GetPinFunctions::send(EmbMessenger* messenger)
    {
        messenger->write(m_S3mode, m_S5mode);
    }

    void GetPinFunctions::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetDeadBand::SetDeadBand(uint8_t Max)
        : m_Max(Max)
    {}

    void SetDeadBand::send(EmbMessenger* messenger)
    {
        messenger->write(m_Max);
    }

    void SetDeadBand::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    GetDeadBand::GetDeadBand(uint8_t Max)
        : m_Max(Max)
    {}

    void GetDeadBand::send(EmbMessenger* messenger)
    {
        messenger->write(m_Max);
    }

    void GetDeadBand::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadEncoders::ReadEncoders(uint32_t enc2)
        : m_enc2(enc2)
    {}

    void ReadEncoders::send(EmbMessenger* messenger)
    {
        messenger->write(m_enc2);
    }

    void ReadEncoders::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadISpeeds::ReadISpeeds(uint32_t ispeed2)
        : m_ispeed2(ispeed2)
    {}

    void ReadISpeeds::send(EmbMessenger* messenger)
    {
        messenger->write(m_ispeed2);
    }

    void ReadISpeeds::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    RestoreDefaults::RestoreDefaults(uint8_t address)
        : m_address(address)
    {}

    void RestoreDefaults::send(EmbMessenger* messenger)
    {
        messenger->write(m_address);
    }

    void RestoreDefaults::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadTemp::ReadTemp(uint16_t temp)
        : m_temp(temp)
    {}

    void ReadTemp::send(EmbMessenger* messenger)
    {
        messenger->write(m_temp);
    }

    void ReadTemp::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadTemp2::ReadTemp2(uint16_t temp)
        : m_temp(temp)
    {}

    void ReadTemp2::send(EmbMessenger* messenger)
    {
        messenger->write(m_temp);
    }

    void ReadTemp2::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadError::ReadError(bool* valid)
        : m_valid(valid)
    {}

    void ReadError::send(EmbMessenger* messenger)
    {
        messenger->write(m_valid);
    }

    void ReadError::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadEncoderModes::ReadEncoderModes(uint8_t M2mode)
        : m_M2mode(M2mode)
    {}

    void ReadEncoderModes::send(EmbMessenger* messenger)
    {
        messenger->write(m_M2mode);
    }

    void ReadEncoderModes::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM1EncoderMode::SetM1EncoderMode(uint8_t mode)
        : m_mode(mode)
    {}

    void SetM1EncoderMode::send(EmbMessenger* messenger)
    {
        messenger->write(m_mode);
    }

    void SetM1EncoderMode::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM2EncoderMode::SetM2EncoderMode(uint8_t mode)
        : m_mode(mode)
    {}

    void SetM2EncoderMode::send(EmbMessenger* messenger)
    {
        messenger->write(m_mode);
    }

    void SetM2EncoderMode::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    WriteNVM::WriteNVM(uint8_t address)
        : m_address(address)
    {}

    void WriteNVM::send(EmbMessenger* messenger)
    {
        messenger->write(m_address);
    }

    void WriteNVM::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadNVM::ReadNVM(uint8_t address)
        : m_address(address)
    {}

    void ReadNVM::send(EmbMessenger* messenger)
    {
        messenger->write(m_address);
    }

    void ReadNVM::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetConfig::SetConfig(uint16_t config)
        : m_config(config)
    {}

    void SetConfig::send(EmbMessenger* messenger)
    {
        messenger->write(m_config);
    }

    void SetConfig::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    GetConfig::GetConfig(uint16_t config)
        : m_config(config)
    {}

    void GetConfig::send(EmbMessenger* messenger)
    {
        messenger->write(m_config);
    }

    void GetConfig::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM1MaxCurrent::SetM1MaxCurrent(uint32_t max)
        : m_max(max)
    {}

    void SetM1MaxCurrent::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void SetM1MaxCurrent::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetM2MaxCurrent::SetM2MaxCurrent(uint32_t max)
        : m_max(max)
    {}

    void SetM2MaxCurrent::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void SetM2MaxCurrent::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadM1MaxCurrent::ReadM1MaxCurrent(uint32_t max)
        : m_max(max)
    {}

    void ReadM1MaxCurrent::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void ReadM1MaxCurrent::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    ReadM2MaxCurrent::ReadM2MaxCurrent(uint32_t max)
        : m_max(max)
    {}

    void ReadM2MaxCurrent::send(EmbMessenger* messenger)
    {
        messenger->write(m_max);
    }

    void ReadM2MaxCurrent::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    SetPWMMode::SetPWMMode(uint8_t mode)
        : m_mode(mode)
    {}

    void SetPWMMode::send(EmbMessenger* messenger)
    {
        messenger->write(m_mode);
    }

    void SetPWMMode::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }

    GetPWMMode::GetPWMMode(uint8_t mode)
        : m_mode(mode)
    {}

    void GetPWMMode::send(EmbMessenger* messenger)
    {
        messenger->write(m_mode);
    }

    void GetPWMMode::receive(EmbMessenger* messenger)
    {
        messenger->read(m_rv);
    }
    */
}
