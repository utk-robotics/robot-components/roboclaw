// #include "appendages/roboclaw.hpp"
#include <chrono>
#include <ctime>
#include <memory>
#include <thread>
#include <tuple>
#include <utility>
#include "rip/components/roboclaw_arduino.hpp"
#include "rip/components/embed_commands.hpp"
namespace rip::components
{
    RoboclawArduino::RoboclawArduino(std::shared_ptr<EmbMessenger> m, uint8_t address)
    {
        m_address = address;
        m_messenger = m;
        if(!m_registered)
        {
            registerCommands();
            m_registered = true;
        }
    }

    RoboclawArduino::RoboclawArduino(const std::string& name, const nlohmann::json& config, 
        std::shared_ptr<emb::host::EmbMessenger> emb, 
        std::shared_ptr<std::unordered_map<std::string, 
        std::shared_ptr<RobotComponent> > > comps)
        : Roboclaw(name, config, emb, comps)
    {
        m_messenger = emb;
    }
        

    void RoboclawArduino::stop()
    {
        DutyM1M2(0, 0);
    }

    void RoboclawArduino::registerCommands()
    {
        // TODO: Ensure that the command ID's are registered thru json &
        // command map in the near future.
        m_messenger->registerCommand<commands::DutyM1>(1);
        m_messenger->registerCommand<commands::DutyM2>(2);
        m_messenger->registerCommand<commands::DutyM1M2>(3);
    }

    std::vector< std::string > RoboclawArduino::diagnosticCommands() const
    {
        std::vector< std::string > s;
        return s;
    }

    RoboclawArduino::~RoboclawArduino()
    {
    }
    /*
    bool RoboclawArduino::ForwardM1(uint8_t speed) {}

    bool RoboclawArduino::BackwardM1(uint8_t speed) {}

    bool RoboclawArduino::SetMinVoltageMainBattery(uint8_t voltage) {}

    bool RoboclawArduino::SetMaxVoltageMainBattery(uint8_t voltage) {}

    bool RoboclawArduino::ForwardM2(uint8_t speed) {}

    bool RoboclawArduino::BackwardM2(uint8_t speed) {}

    bool RoboclawArduino::ForwardBackwardM1(uint8_t speed) {}

    bool RoboclawArduino::ForwardBackwardM2(uint8_t speed) {}

    bool RoboclawArduino::ForwardMixed(uint8_t speed) {}

    bool RoboclawArduino::BackwardMixed(uint8_t speed) {}

    bool RoboclawArduino::TurnRightMixed(uint8_t speed) {}

    bool RoboclawArduino::TurnLeftMixed(uint8_t speed) {}

    bool RoboclawArduino::ForwardBackwardMixed(uint8_t speed) {}

    bool RoboclawArduino::LeftRightMixed(uint8_t speed) {}

    uint32_t RoboclawArduino::ReadEncM1(uint8_t *status = NULL, bool *valid = NULL) {}

    uint32_t RoboclawArduino::ReadEncM2(uint8_t *status = NULL, bool *valid = NULL) {}

    bool RoboclawArduino::SetEncM1(int32_t val) {}

    bool RoboclawArduino::SetEncM2(int32_t val) {}

    uint32_t RoboclawArduino::ReadSpeedM1(uint8_t *status = NULL, bool *valid = NULL) {}

    uint32_t RoboclawArduino::ReadSpeedM2(uint8_t *status = NULL, bool *valid = NULL) {}

    bool RoboclawArduino::ResetEncoders(uint8_t address) {}

    bool RoboclawArduino::ReadVersion(char *version) {}

    uint16_t RoboclawArduino::ReadMainBatteryVoltage(bool *valid = NULL) {}

    uint16_t RoboclawArduino::ReadLogicBatteryVoltage(bool *valid = NULL) {}

    bool RoboclawArduino::SetMinVoltageLogicBattery(uint8_t voltage) {}

    bool RoboclawArduino::SetMaxVoltageLogicBattery(uint8_t voltage) {}

    bool RoboclawArduino::SetM1VelocityPID(float Kp, float Ki, float Kd, uint32_t qpps) {}

    bool RoboclawArduino::SetM2VelocityPID(float Kp, float Ki, float Kd, uint32_t qpps) {}

    uint32_t RoboclawArduino::ReadISpeedM1(uint8_t *status = NULL, bool *valid = NULL) {}

    uint32_t RoboclawArduino::ReadISpeedM2(uint8_t *status = NULL, bool *valid = NULL) {}
    */
    bool RoboclawArduino::DutyM1(uint16_t duty) 
    {
        auto cmd = std::make_shared<commands::DutyM1>(m_address, duty);
        m_messenger->send(cmd);
        cmd->wait();

        bool ret;
        // m_messenger->read<bool>(ret);
        return false;
    }

    bool RoboclawArduino::DutyM2(uint16_t duty) 
    {
        auto cmd = std::make_shared<commands::DutyM2>(m_address, duty);
        m_messenger->send(cmd);
        // bool ret;
        cmd->wait();

        // m_messenger->read<bool>(ret);
        return false;
    }

    bool RoboclawArduino::DutyM1M2(uint16_t duty1, uint16_t duty2) 
    {
        auto cmd = std::make_shared<commands::DutyM1M2>(m_address, duty1, duty2);
        m_messenger->send(cmd);
        cmd->wait();
        // m_messenger->read<bool>(ret);
        return false;
    }
    /*  
    bool RoboclawArduino::SpeedM1(uint32_t speed) {}

    bool RoboclawArduino::SpeedM2(uint32_t speed) {}

    bool RoboclawArduino::SpeedM1M2(uint32_t speed1, uint32_t speed2) {}

    bool RoboclawArduino::SpeedAccelM1(uint32_t accel, uint32_t speed) {}

    bool RoboclawArduino::SpeedAccelM2(uint32_t accel, uint32_t speed) {}

    bool RoboclawArduino::SpeedAccelM1M2(uint32_t accel, uint32_t speed1, uint32_t speed2) {}

    bool RoboclawArduino::SpeedDistanceM1(uint32_t speed, uint32_t distance, uint8_t flag = 0) {}

    bool RoboclawArduino::SpeedDistanceM2(uint32_t speed, uint32_t distance, uint8_t flag = 0) {}

    bool RoboclawArduino::SpeedDistanceM1M2(uint32_t speed1,
                                    uint32_t distance1,
                                    uint32_t speed2,
                                    uint32_t distance2,
                                    uint8_t flag = 0) {}

    bool RoboclawArduino::SpeedAccelDistanceM1(uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag = 0) {}

    bool RoboclawArduino::SpeedAccelDistanceM2(uint32_t accel, uint32_t speed, uint32_t distance, uint8_t flag = 0) {}

    bool RoboclawArduino::SpeedAccelDistanceM1M2(uint32_t accel,
                                        uint32_t speed1,
                                        uint32_t distance1,
                                        uint32_t speed2,
                                        uint32_t distance2,
                                        uint8_t flag = 0) {}

    bool RoboclawArduino::ReadBuffers(uint8_t &depth1, uint8_t &depth2) {}

    bool RoboclawArduino::ReadPWMs(int16_t &pwm1, int16_t &pwm2) {}

    bool RoboclawArduino::ReadCurrents(int16_t &current1, int16_t &current2) {}

    bool RoboclawArduino::SpeedAccelM1M2_2(uint32_t accel1, uint32_t speed1, uint32_t accel2, uint32_t speed2) {}

    bool RoboclawArduino::SpeedAccelDistanceM1M2_2(uint32_t accel1,
                                            uint32_t speed1,
                                            uint32_t distance1,
                                            uint32_t accel2,
                                            uint32_t speed2,
                                            uint32_t distance2,
                                            uint8_t flag = 0) {}

    bool RoboclawArduino::DutyAccelM1(uint16_t duty, uint32_t accel) {}

    bool RoboclawArduino::DutyAccelM2(uint16_t duty, uint32_t accel) {}

    bool RoboclawArduino::DutyAccelM1M2(uint16_t duty1, uint32_t accel1, uint16_t duty2, uint32_t accel2) {}

    bool RoboclawArduino::ReadM1VelocityPID(float &Kp_fp, float &Ki_fp, float &Kd_fp, uint32_t &qpps) {}

    bool RoboclawArduino::ReadM2VelocityPID(float &Kp_fp, float &Ki_fp, float &Kd_fp, uint32_t &qpps) {}

    bool RoboclawArduino::SetMainVoltages(uint16_t min, uint16_t max) {}

    bool RoboclawArduino::SetLogicVoltages(uint16_t min, uint16_t max) {}

    bool RoboclawArduino::ReadMinMaxMainVoltages(uint16_t &min, uint16_t &max) {}

    bool RoboclawArduino::ReadMinMaxLogicVoltages(uint16_t &min, uint16_t &max) {}

    bool RoboclawArduino::SetM1PositionPID(float kp,
                                    float ki,
                                    float kd,
                                    uint32_t kiMax,
                                    uint32_t deadzone,
                                    uint32_t min,
                                    uint32_t max) {}

    bool RoboclawArduino::SetM2PositionPID(float kp,
                                    float ki,
                                    float kd,
                                    uint32_t kiMax,
                                    uint32_t deadzone,
                                    uint32_t min,
                                    uint32_t max) {}

    bool RoboclawArduino::ReadM1PositionPID(float &Kp,
                                    float &Ki,
                                    float &Kd,
                                    uint32_t &KiMax,
                                    uint32_t &DeadZone,
                                    uint32_t &Min,
                                    uint32_t &Max) {}

    bool RoboclawArduino::ReadM2PositionPID(float &Kp,
                                    float &Ki,
                                    float &Kd,
                                    uint32_t &KiMax,
                                    uint32_t &DeadZone,
                                    uint32_t &Min,
                                    uint32_t &Max) {}

    bool RoboclawArduino::SpeedAccelDeccelPositionM1(uint32_t accel,
                                            uint32_t speed,
                                            uint32_t deccel,
                                            uint32_t position,
                                            uint8_t flag) {}

    bool RoboclawArduino::SpeedAccelDeccelPositionM2(uint32_t accel,
                                            uint32_t speed,
                                            uint32_t deccel,
                                            uint32_t position,
                                            uint8_t flag) {}

    bool RoboclawArduino::SpeedAccelDeccelPositionM1M2(uint32_t accel1,
                                                uint32_t speed1,
                                                uint32_t deccel1,
                                                uint32_t position1,
                                                uint32_t accel2,
                                                uint32_t speed2,
                                                uint32_t deccel2,
                                                uint32_t position2,
                                                uint8_t flag) {}

    bool RoboclawArduino::SetM1DefaultAccel(uint32_t accel) {}

    bool RoboclawArduino::SetM2DefaultAccel(uint32_t accel) {}

    bool RoboclawArduino::SetPinFunctions(uint8_t S3mode, uint8_t S4mode, uint8_t S5mode) {}

    bool RoboclawArduino::GetPinFunctions(uint8_t &S3mode, uint8_t &S4mode, uint8_t &S5mode) {}

    bool RoboclawArduino::SetDeadBand(uint8_t Min, uint8_t Max) {}

    bool RoboclawArduino::GetDeadBand(uint8_t &Min, uint8_t &Max) {}

    bool RoboclawArduino::ReadEncoders(uint32_t &enc1, uint32_t &enc2) {}

    bool RoboclawArduino::ReadISpeeds(uint32_t &ispeed1, uint32_t &ispeed2) {}

    bool RoboclawArduino::RestoreDefaults(uint8_t address) {}

    bool RoboclawArduino::ReadTemp(uint16_t &temp) {}

    bool RoboclawArduino::ReadTemp2(uint16_t &temp) {}

    uint16_t RoboclawArduino::ReadError(bool *valid = NULL) {}

    bool RoboclawArduino::ReadEncoderModes(uint8_t &M1mode, uint8_t &M2mode) {}

    bool RoboclawArduino::SetM1EncoderMode(uint8_t mode) {}

    bool RoboclawArduino::SetM2EncoderMode(uint8_t mode) {}

    bool RoboclawArduino::WriteNVM(uint8_t address) {}

    bool RoboclawArduino::ReadNVM(uint8_t address) {}

    bool RoboclawArduino::SetConfig(uint16_t config) {}

    bool RoboclawArduino::GetConfig(uint16_t &config) {}

    bool RoboclawArduino::SetM1MaxCurrent(uint32_t max) {}

    bool RoboclawArduino::SetM2MaxCurrent(uint32_t max) {}

    bool RoboclawArduino::ReadM1MaxCurrent(uint32_t &max) {}

    bool RoboclawArduino::ReadM2MaxCurrent(uint32_t &max) {}

    bool RoboclawArduino::SetPWMMode(uint8_t mode) {}

    bool RoboclawArduino::GetPWMMode(uint8_t &mode) {}
    */
}
